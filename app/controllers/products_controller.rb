class ProductsController < ApplicationController

  def filters

    logger.debug params[:category_id]
    
    
    @category = Category.find(params[:category_id])
    if @category.root?
      @categories = @category.children.pluck(:id)
      @products = Product.includes(:categories, :photos).where(categories: {id: @categories})
    else
      @products = @category.products.includes(:photos)
    end

    @products = @products.where(price: params[:price_min]..params[:price_max])
    

    def prod_val(products, filters)
      @p = []
      if filters.length > 0
        f = filters.shift
        for product in products
          if product.values.exists?(f)
            @p.push product
          end
        end
        @p = @p.uniq
        prod_val(@p, filters)
      else
        return @p = products
      end
    end


    if params[:filters]
      filters = []
      JSON.parse(params[:filters]).each do |filter|
        if filter[1]
          filters.push filter[1]
        end
      end
      @products = @products.filter(filters.join(","))
      @products = prod_val(@products, filters)
    end


    @res = []
    @products.each do |product|
      prod = {product: product, full_path: product.full_path, photo: product.photos.first.photo.thumb.url}
      @res.push prod
    end

    render :json => @res
  end


  def catalog
    @products = Product.all
  end

  def category
    @category = Category.find_by_path(params[:id])
    @products = @category.products
  end

  def product
    @product = Product.includes(:categories, :photos, :values, :properties).find(params[:product_id])
    @properties = @product.properties.includes(:values)
    @values = @product.values
    @category = @product.categories.first
  end

  private
  def product_params
    params.require(:product).permit(:price, :price_min, :price_max, :title, :body, :filters, :product_id, :category_id, :gallery_id)
  end

end
