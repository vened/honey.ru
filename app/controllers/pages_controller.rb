class PagesController < ApplicationController

  def index
  end

  def show
    @page = Page.includes(:galleries).find_by_path(params[:id])
  end

end
