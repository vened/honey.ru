# require './lib/setting'
class Admin::SettingsController < Admin::AdminController
  before_action :signed_in_manager
  skip_before_action :verify_authenticity_token

  def index
    @settings = Setting.all
    render :json => @settings
  end

  def update
    Setting.update(params[:setting])
    @settings = Setting.all
    render :json => @settings
  end
  
end
