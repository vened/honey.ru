class OrdersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def add_to_cart
    @product = Product.find(params[:product_id])

    if Order.find_by_session_id(session[:session_id])
      @order = Order.find_by_session_id(session[:session_id])
    else
      @order = Order.create(session_id: session[:session_id])
    end
    
    if @order.products.exists?(@product)
      @order_product = OrderProduct.find_by(product_id: @product.id, order_id: @order.id)
      @order_product.count += 1
      @order_product.save
    else
      @order.products << @product
    end

    @mini_cart = Order.mini_cart(session[:session_id])
    
    render :json => @mini_cart
  end

  def mini_cart
    @mini_cart = Order.mini_cart(session[:session_id])
    render :json => @mini_cart
  end
  
  def all_cart
    @cart = Order.find_by_session_id(session[:session_id])
    @cart_items = OrderProduct.where(order_id: @cart.id)
    @cart_price = Order.order_price(@cart)
  end
  
  def cart
  end

end
