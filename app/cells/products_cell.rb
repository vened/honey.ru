class ProductsCell < Cell::Rails

  def list
    @products = Product.all.first(4)
    render
  end

end
