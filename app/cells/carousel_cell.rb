class CarouselCell < Cell::Rails

  def carousel
    @carousel = Gallery.includes(:photos).find_by_slider(true)
    @photos = @carousel.photos
    render
  end

end
