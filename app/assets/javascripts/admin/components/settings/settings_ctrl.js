(function ()
{
	"use strict"

	var settings = angular.module("settingsControllers", []);

	settings.controller('SettingsIndexCtrl', [
		'$rootScope',
		'$scope',
		'SettingsService',
		'allSettings',
		function ($rootScope, $scope, SettingsService, allSettings)
		{
			$scope.settings = allSettings.data
			$scope.save = function ()
			{
				SettingsService.update($scope.settings).success(function (data)
				{
					$scope.settings = data;
					$rootScope.$broadcast('alert', "Ностройки успешно сохранены!");
				})
			}
		}
	]);

}());
