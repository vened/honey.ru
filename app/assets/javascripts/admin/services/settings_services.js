(function ()
{
	"use strict"

	angular.module('settingsServices', [])


	function SettingsService($http)
	{
		return{
			all   : function ()
			{
				return $http.get("/admin/settings/all")
			},
			update   : function (obj)
			{
				return $http.put("/admin/settings/update", obj)
			}
		}
	}


    SettingsService.$inject = ['$http'];
	angular
		.module('settingsServices')
		.service('SettingsService', SettingsService);


})()