(function ()
{
	"use strict"

	var orders = angular.module("orders", []);

	orders.factory('Order', [
		'$http',
		function ($http)
		{
			return {
				addToCart: function (obj)
				{
					return $http.post("/order/cart_add", obj);
				},
				mini_cart     : function ()
				{
					return $http.get("/order/mini_cart");
				},
				all_cart     : function ()
				{
					return $http.get("/order/all_cart.json");
				}
			}
		}
	]);

	orders.controller('OrderCtrl', [
		'$scope',
		'Order',
		function ($scope, Order)
		{

			Order.mini_cart().success(function (data)
			{
				$scope.cartItems = data;
			})

			$scope.addOrder = function (id)
			{
				Order.addToCart({product_id: id}).success(function (data)
				{
					console.log(data);
					$scope.cartItems = data;
				})
			}

		}
	]);

	orders.controller('CartCtrl', [
		'$scope',
		'Order',
		function ($scope, Order)
		{
			
			$scope.orderGet = false;

			Order.all_cart().success(function (data)
			{
				$scope.cardData = data;
			})


		}
	]);

}())


