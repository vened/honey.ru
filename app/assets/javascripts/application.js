// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require bootstrap
// require turbolinks
//= require angular
//= require angular-resource
// require angular-ui-bootstrap-bower
//= require magnific-popup
//= require_directory ./site

//Turbolinks.enableProgressBar();

$(document).ready(function ()
{
	$('.photo-gallery').magnificPopup({
		delegate : 'a.insert-img',
		type     : 'image',
		tLoading : 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery  : {
			enabled           : true,
			navigateByImgClick: true,
			preload           : [
				0,
				1
			]
		},
		image    : {
			tError  : '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function (item)
			{
				return item.el.attr('title');
			}
		}
	});
});