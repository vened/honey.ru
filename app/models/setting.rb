require 'yaml'

class Setting

  class << self

    def all
      load_settings
    end

    def get(key)
      load_settings[key.to_s]
    end

    def update(params)
      settings = load_settings
      params.each do |key, value|
        key = key.to_s
        settings[key] = value.to_s
      end
      save_settings(settings)
    end

    def load_settings
      @config ||= YAML.load(File.read(file_path))
    end

    private

    def save_settings(settings)
      File.write(file_path, settings.to_yaml)
    end

    def file_path
      #Rails.root.join('config', CONFIG_FILE)
      "config/settings.yml"
    end

  end

end
