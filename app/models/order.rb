class Order < ActiveRecord::Base
  has_many :order_products
  has_many :products, through: :order_products

  serialize :product, JSON

  def self.order_price(order)
    price = 0
    cart_items = OrderProduct.where(order_id: order.id).includes(:product)
    cart_items.each do |item|
      if item.product.sale
        q = 1 - item.product.sale / 100.0
        product_price = item.product.price * q * item.count
      else
        product_price = item.product.price * item.count
      end
      price += product_price
    end
    return price
  end


  def self.order_count(order)
    count = 0
    cart_items = OrderProduct.where(order_id: order.id).includes(:product)
    cart_items.each do |item|
      count += item.count
    end
    return count
  end


  def self.mini_cart(session_id)
    order = where(session_id: session_id).includes(:products)
    return {price: order.order_price(order[0]), count: order.order_count(order[0])}
  end

end
