json.cart @cart_items do |item|
  json.product_photo item.product.photos.first.photo.thumb.url
  json.product_name item.product.name
  json.sale item.product.sale
  json.product_price_old item.product.price
  json.product_price item.product.price_sale
  json.count item.count
  json.price item.product.price_sale * item.count
end
json.price @cart_price
