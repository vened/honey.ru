class CreateTableOrderProducts < ActiveRecord::Migration
  def change
    create_table :order_products do |t|
      t.belongs_to :order, index: true
      t.belongs_to :product, index: true
      t.integer :count, :null => false, :default => 1
    end
  end
end
