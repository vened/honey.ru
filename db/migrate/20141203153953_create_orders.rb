class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :session_id
      t.string :name
      t.string :phone
      t.string :email
      t.text :address
      t.text :comment
      t.decimal :price, :null => false, :default => 0
      t.integer :status, :null => false, :default => 1
      
      t.timestamps
    end
    add_index :orders, :session_id
  end
end
