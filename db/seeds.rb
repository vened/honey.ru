# rake db:drop db:create db:migrate db:seed
# require 'parser/parser'
I18n.locale = :ru


@manager = Manager.create(name: "admin", email: "maxstbn@gmail.com", password: "nfnkfrf", password_confirmation: "nfnkfrf")


Page.create(
    name: "О компании",
    path: "o-kompanii",
    body: '<p>Мед</p>'
)

Page.create(
    name: "Доставка",
    path: "dostavka",
    body: "О доставке"
)

Page.create(
    name: "Главная",
    path: "root",
    body: ""
)



@slider = Gallery.create(:name => "Слайдер в шапке", :slider => true)
@slider_photo = Photo.create(photo: File.open(Rails.root.to_s + "/public/honey/1.jpg"), name: "Богатый выбор для вашего интерьера")
@slider.photos << @slider_photo


@catalog = [
    {name: "Мёд", parent_name: nil}
]

@catalog.each do |item|
  path = I18n.transliterate(item[:name])
  path = path.gsub(/\s/, '-')
  parent = item[:parent_name]
  if parent != nil
    @parent = Category.find_by_name(parent)
    @category = Category.create(name: item[:name], path: path.downcase, parent_id: @parent.id)
  else
    @category = Category.create(name: item[:name], path: path.downcase)
  end
end